package movies.importer;
import java.io.*;
public class ImportPipeline {
	/**
	 * Processor[] created in the main method 
	 * with all the processors
	 * @author Leon Hazurin, Shiv Patel
	 */
	public static void main(String[] args) {
		//All the different sources used in each processors, edit them depending on your own sources
		String fileInput = "/Users/mukeshpatel/Documents/Movie_Processor/Texts/Kaggle";
		String fileImdbInput = "/Users/mukeshpatel/Documents/Movie_Processor/Texts/Imdb";
		String fileImported = "/Users/mukeshpatel/Documents/Movie_Processor/Texts/Imported";
		String fileNormalized = "/Users/mukeshpatel/Documents/Movie_Processor/Texts/Normalized";
		String fileDeduped = "/Users/mukeshpatel/Documents/Movie_Processor/Texts/Depuded";
		
		
		ImportPipeline mainApp = new ImportPipeline();
		
		//Creating all the processors
		Processor[] pro = new Processor[5];
		pro[0] = new KaggleImporter(fileInput, fileImported);
		pro[1] = new Normalizer(fileImported, fileNormalized);
		pro[2] = new ImdbImporter(fileImdbInput, fileImported);
		pro[3] = new Validator(fileImported,fileNormalized);
		pro[4] = new Deduper(fileNormalized, fileDeduped);
		
		try {
			mainApp.processAll(pro);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * execute() method is called on 
	 * all the entries within the Processor[]
	 * @author Leon Hazurin, Shiv Patel
	 * 
	 */
	public void processAll(Processor[] in) throws IOException{
		for(Processor x : in) {
			x.execute();
			System.out.println("Processed");
		}
	}
}

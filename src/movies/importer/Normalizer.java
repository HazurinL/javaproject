package movies.importer;
import java.util.ArrayList;
public class Normalizer extends Processor{
	public Normalizer(String source, String out) {
		super(source, out, false);
	}
	/**
	 * method normalizes the imported arraylist by 
	 * reducing the length(if needed0 of the runtime
	 * and applies the toLowerCase method on all titles 
	 * within the arraylist.
	 * @author 1640570 Leon
	 */
	public ArrayList<String> process(ArrayList<String> in){
		int i = 0;
		for (String entry : in) {
			if (entry.split(",\t").length == 4) {
				String[] splitEntry = entry.split(",\t");
				String title = splitEntry[1];
				String minutes = splitEntry[2];
				title = title.toLowerCase();
				if(minutes.split(" ").length == 2) {
					minutes = minutes.split(" ")[0];
				}
				entry = splitEntry[0] + ",\t" + title + ",\t" + minutes + ",\t" + splitEntry[3];
				in.set(i, entry);
				title = "";
				minutes = "";
			}
			i++;
		}
		return in;
	}
}
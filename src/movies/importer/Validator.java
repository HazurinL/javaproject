package movies.importer;
import java.util.ArrayList;
public class Validator extends Processor {
	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/**
	 * Method process will take in an ArrayList<String> which will contain the imdb file that was processed in the ImdbImporter class.
	 * The method will loop through this ArrayList<String> and will only add data that satisfies specific conditions
	 * to the new ArrayList<String> which will then be returned.
	 * @author 1935098 Shiv Patel
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		//Initializing all the variables needed in order for the method to do its job
		ArrayList<String> asRemoved = new ArrayList<String>();
		final int year = 0;
		final int name = 1;
		final int duration = 2;
		final int source = 3;
		boolean remove = false;
		String[] movie;
		
		//For loop will go through the input ArrayList
		for (int i=0; i < input.size(); i++) {
			movie = input.get(i).split(",\\t"); 
			Movie imdb_movies = new Movie(movie[year], movie[name], movie[duration], movie[source]);
			
			//Following if statement makes sure there are no null and empty values in the Movie Object
			if (imdb_movies.getName() == null || imdb_movies.getReleaseYear() == null || imdb_movies.getRunTime() == null || imdb_movies.getSource() == null) {
				remove = true;
			}
			else if (imdb_movies.getName() == "" || imdb_movies.getReleaseYear() == "" || imdb_movies.getRunTime() == "" || imdb_movies.getSource() == "") {
				remove = true;
			}
			//If every value is there we do the try catch code where we make sure the releaseYear and Duration strings can be turned into ints
			else {
				try {
					Integer.parseInt(imdb_movies.getReleaseYear());
					Integer.parseInt(imdb_movies.getRunTime());
				}
				catch(NumberFormatException e) {
					remove = true;
					System.out.println("The string year/duration could not be converted to an int meaning it is wrong");
				}
			}
			//If statement that will only add the Movie object that were not empty or null
			if (remove == false) {
				asRemoved.add(imdb_movies.toString());
			}
			else {
			}
		}
		return asRemoved;
	}

}

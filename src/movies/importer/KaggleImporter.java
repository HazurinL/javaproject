package movies.importer;
import java.util.ArrayList;
public class KaggleImporter extends Processor{
	public KaggleImporter(String source, String out) {
		super(source, out, true);
	}
	/**
	 * method extracts the needed information from an arraylist
	 * returns the arraylist with only the needed fields remaining
	 * entries with the wrong amount of elements after split() 
	 * remain the same.
	 * @author 1640570 Leon
	 * 
	 */
	public ArrayList<String> process(ArrayList<String> in){
		int i = 0;
		for(String element : in) {
			if(element.split("\t").length == 21) {
				String temp;
				String[] strArr = element.split("\t");
				temp = strArr[20] + ",\t" + strArr[15] + ",\t" + strArr[13] + ",\t" + "kaggle";
				in.set(i, temp);
				i++;
				temp = "";
			}else {
				i++;
				continue;
			}
		}
		return in;
	}
}
package movies.importer;
import java.util.ArrayList;
public class Deduper extends Processor {
	public Deduper(String in, String out) {
		super(in,out, false);
	}
	/**
	 * after the arraylist has been imported and normalized
	 * this method removes the duplicates. If the title, year of release
	 * and the runtime is within 5 minutes of difference for both movies
	 * a merged movie is created
	 * @author  Leon Hazurin, Shiv Patel
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> refined = new ArrayList<String>();
		
		//2 For loops that will put int input ArrayList into two different strings so we can compare
		for (String temp : input) {
			for (String t : input) {
				
				//if statement checks whether or not the title and release year match, it also checks if the duration is within a 5 minute range 
				if (temp.split(",\t")[0].equalsIgnoreCase(t.split(",\t")[0]) && temp.split(",\t")[1].equalsIgnoreCase(t.split(",\t")[1]) && Integer.parseInt(temp.split(",\t")[2]) - Integer.parseInt(t.split(",\t")[2]) <= 5 && Integer.parseInt(temp.split(",\t")[2]) - Integer.parseInt(t.split(",\t")[2]) >= 0 || Integer.parseInt(temp.split(",\t")[2]) - Integer.parseInt(t.split(",\t")[2]) >= -5 && Integer.parseInt(temp.split(",\t")[2]) - Integer.parseInt(t.split(",\t")[2]) <= 0) {
					
					//If they have the same source we will simply add it normally, other wise we add the second source
					if (temp.split(",\t")[3].equalsIgnoreCase(t.split(",\t")[3])) {
						//If statement makes sure that we truly do not have it in the ArrayList already
						if (refined.contains(temp)) {
						}
						else {
							refined.add(temp);
						}
					}
					else {
						//If statement makes sure that we truly do not have it in the ArrayList already
						if (refined.contains(temp)) {
						}
						else {
							refined.add(temp.split(",\t")[0] + ",\t" + temp.split(",\t")[1] + ",\t" + temp.split(",\t")[2] + ",\t" + temp.split(",\t")[3] + ":" + t.split(",\t")[3]);
						}
					}
				}
			}
		}
		return refined;
	}
}

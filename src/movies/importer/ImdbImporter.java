package movies.importer;
import java.util.ArrayList;
public class ImdbImporter extends Processor {
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	/**
	 * Method process will take in an ArrayList<String> which will contain the raw imdb file information
	 * Then it will turn it into a more refined ArrayList<String> containing 
	 * only the year,name,duration as information for each movie which will be returned.
	 * @author 1935098 Shiv Patel
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		
		//Constant int that are used for specific columns
		final int name = 1;
		final int year = 3;
		final int duration = 6;
		ArrayList<String> transformed = new ArrayList<String>();
		String[] movie;
		
		//For loop will go through the input arraylist and convert it to the Movie object format and put it in a new ArrayList
		for (int i=0; i < input.size(); i++) {
			movie = input.get(i).split("\\t", -1);
			Movie imdb_movies = new Movie(movie[year], movie[name], movie[duration], "imdb");
			transformed.add(imdb_movies.toString());
		}
		return transformed;
	}

}

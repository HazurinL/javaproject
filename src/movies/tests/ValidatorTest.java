package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Validator;

class ValidatorTest {

	@Test
	void goodValueTest() {
		Validator validate = new Validator("", "");
		ArrayList<String> processed = new ArrayList<String>();
		processed.add("1894,	Miss Jerry,	45,	imdb");
		processed.add("1906,	The Story of the Kelly Gang,	70,	imdb");
		ArrayList<String> removed = new ArrayList<String>();
		removed.add("1894,	Miss Jerry,	45,	imdb");
		removed.add("1906,	The Story of the Kelly Gang,	70,	imdb");
		assertEquals(validate.process(processed), removed);
	}
	
	@Test
	void emptyValueTest() {
		Validator validate = new Validator("", "");
		ArrayList<String> processed = new ArrayList<String>();
		processed.add("1894,	Miss Jerry,	45,	imdb");
		processed.add(" ,	The Story of the Kelly Gang,	70,	imdb");
		ArrayList<String> asRemoved = new ArrayList<String>();
		asRemoved.add("1894,	Miss Jerry,	45,	imdb");
		assertEquals(validate.process(processed), asRemoved);
	}
	
	@Test
	void nullValueTest() {
		Validator validate = new Validator("", "");
		ArrayList<String> processed = new ArrayList<String>();
		processed.add("1894,	Miss Jerry,	45,	imdb");
		processed.add(",	The Story of the Kelly Gang,	70,	imdb");
		ArrayList<String> asRemoved = new ArrayList<String>();
		asRemoved.add("1894,	Miss Jerry,	45,	imdb");
		assertEquals(validate.process(processed), asRemoved);
	}
	
}

package movies.tests;
import movies.importer.*;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class NormalizerTest {

	@Test
	void test() {
		ArrayList<String> in = new ArrayList<String>();
		in.add("1957,	Heaven Knows Mr. Allison,	108 minutes,	kaggle");
		in.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Brendan Fraser	John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi	Rob Cohen	Simon Duggan	Director Not Available	Action	PG-13 	7/24/2008	112 minutes	Universal Pictures	The Mummy: Tomb of the Dragon Emperor	Alfred Gough	Miles Millar	Writer Not Available	Writer Not Available	2008");
		in.add("1979,	Apocalypse Now,	minutes 153,	kaggle");
		in.add("1975,	The Fortune,	88,	kaggle");
		in.add("2017,	The Little Vampire 3D,	82 minutes,	kaggle");
		Normalizer norm = new Normalizer("","");
		ArrayList<String> test = norm.process(in);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("1957,	heaven knows mr. allison,	108,	kaggle");
		expected.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Brendan Fraser	John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi	Rob Cohen	Simon Duggan	Director Not Available	Action	PG-13 	7/24/2008	112 minutes	Universal Pictures	The Mummy: Tomb of the Dragon Emperor	Alfred Gough	Miles Millar	Writer Not Available	Writer Not Available	2008");
		expected.add("1979,	apocalypse now,	minutes,	kaggle");
		expected.add("1975,	the fortune,	88,	kaggle");
		expected.add("2017,	the little vampire 3d,	82,	kaggle");
		assertEquals(test, expected);
	}

}

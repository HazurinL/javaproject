package movies.tests;
import movies.importer.*;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MovieTest {
	
	 @Test
	 void testGetReleaseYear() {
		Movie movie = new Movie("2007", "batman", "270 minutes", "imdb");
		String year = movie.getReleaseYear();
		assertTrue(year=="2007");
	}
	
	@Test
	 void testGetName() {
		Movie movie = new Movie("2007", "batman", "270 minutes", "imdb");
		String name = movie.getName();
		assertTrue(name=="batman");
	}
	
	@Test
	 void testGetRunTime() {
		Movie movie = new Movie("2007", "batman", "270 minutes", "imdb");
		String runTime = movie.getRunTime();
		assertTrue(runTime=="270 minutes");
	}
	
	@Test
	 void testGetSource() {
		Movie movie = new Movie("2007", "batman", "270 minutes", "imdb");
		String source = movie.getSource();
		assertTrue(source=="imdb");
	}
	
	@Test
	void testToString() {
	Movie movie = new Movie("2007", "batman", "270 minutes", "imdb");
	String out = movie.toString();
	String x = "2007,	batman,	270 minutes,	imdb";
	assertTrue(out.equals(x));
	}
}
